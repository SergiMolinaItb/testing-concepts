package cat.itb.myapplication;

import android.app.Activity;

import androidx.test.core.app.ActivityScenario;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.ViewAssertion;
import androidx.test.espresso.action.ViewActions;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import static androidx.test.espresso.Espresso.*;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static com.schibsted.spain.barista.interaction.BaristaEditTextInteractions.writeTo;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    public String USER_TO_BE_TYPED = "sergi";
    public String PASS_TO_BE_TYPED = "1234";

    //ACT 2

    @Rule
    public ActivityScenarioRule<MainActivity> activityActivityScenarioRule = new ActivityScenarioRule<MainActivity>(MainActivity.class);

    @Rule
    public ActivityScenarioRule<SecondActivity> activityActivityScenarioRuleS = new ActivityScenarioRule<SecondActivity>(SecondActivity.class);


    @Test
    public void checkElementDisplayedCorrectly () {
        onView(withId(R.id.text1)).check(matches(isDisplayed()));
        onView(withId(R.id.buttonNext)).check(matches(isDisplayed()));
    }

    @Test
    public void checkTitleIsCorrect () {
        onView(withId(R.id.text1)).check(matches(withText("Main Activity Title")));
        onView(withId(R.id.buttonNext)).check(matches(withText("Next")));
    }

//    @Test
//    public void checkButtonClickable() {
//        onView(withId(R.id.buttonNext)).check(matches(isClickable()));
//        onView(withId(R.id.buttonNext)).perform(click()).check(matches(withText(R.string.back)));
//    }

    //ACT 3

    @Test
    public void login_from_behaviour() {
        onView(withId(R.id.username)).perform(typeText(USER_TO_BE_TYPED)).perform(ViewActions.closeSoftKeyboard());
        onView(withId(R.id.password)).perform(typeText(PASS_TO_BE_TYPED)).perform(ViewActions.closeSoftKeyboard());
        // onView(withId(R.id.buttonNext)).perform(click()).check(matches(withText(R.string.logged)));
    }

    //ACT 4

    @Test
    public void checkIntentNext() {
        onView(withId(R.id.buttonNext)).perform(click());
        onView(withId(R.id.second_activity)).check(matches(isDisplayed()));
    }

    @Test
    public void checkIntentBack() {
        onView(withId(R.id.buttonNext)).perform(click());
        onView(withId(R.id.second_activity)).check(matches(isDisplayed()));
        Espresso.pressBack();
        onView(withId(R.id.main_activity)).check(matches(isDisplayed()));
    }

    //ACT 5

    @Test
    public void largeTest() {
        onView(withId(R.id.username)).perform(typeText(USER_TO_BE_TYPED)).perform(ViewActions.closeSoftKeyboard());
        onView(withId(R.id.password)).perform(typeText(PASS_TO_BE_TYPED)).perform(ViewActions.closeSoftKeyboard());
        onView(withId(R.id.buttonNext)).perform(click());
        onView(withId(R.id.second_activity)).check(matches(isDisplayed()));
        onView(withId(R.id.text2)).check(matches(withText("Welcome back " + USER_TO_BE_TYPED)));
        onView(withId(R.id.buttonBack)).perform(click());
        onView(withId(R.id.main_activity)).check(matches(isDisplayed()));
        onView(withId(R.id.username)).check(matches(withText("")));
        onView(withId(R.id.password)).check(matches(withText("")));
    }

    //ACT 6

    @Test
    public void baristaTest() {
        writeTo(R.id.username, USER_TO_BE_TYPED);
        writeTo(R.id.password, PASS_TO_BE_TYPED);
    }

}
